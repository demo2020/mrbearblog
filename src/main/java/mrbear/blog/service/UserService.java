package mrbear.blog.service;

import mrbear.blog.domain.User;

public interface UserService {

    /**
     * 检查用户名密码
     * @param username
     * @param password
     * @return
     */
    User checkUser(String username,String password);
}
